import Vue from "vue";
import App from "./App.vue";
import Vuex from "vuex";
Vue.config.productionTip = false;
Vue.use(Vuex);
import "./assets/styles/index.css";

const store = new Vuex.Store({
  state: {
    chords: [
      {
        id: 1,
        name: "C",
        category: "Basic",
        fingers: [
          [1, 0],
          [3, 0],
          [6, "x"],
          [2, 1, "1"],
          [4, 2, "2"],
          [5, 3, "3"],
        ],
      },
      {
        id: 2,
        name: "A",
        category: "Basic",
        fingers: [
          [1, 0],
          [5, 0],
          [6, "x"],
          [4, 2, "2"],
          [3, 2, "1"],
          [2, 2, "3"],
        ],
      },
      {
        id: 3,
        name: "E",
        category: "Basic",
        fingers: [
          [1, 0],
          [2, 0],
          [6, 0],
          [3, 1, "1"],
          [4, 2, "3"],
          [5, 2, "2"],
        ],
      },
      {
        id: 4,
        name: "F",
        category: "Basic",
        fingers: [
          [5, "x"],
          [6, "x"],
          [1, 1, "1"],
          [2, 1, "1"],
          [3, 2, "2"],
          [4, 3, "4"],
          [5, 3, "3"],
        ],
      },
      {
        id: 5,
        name: "G",
        category: "Basic",
        fingers: [
          [2, 0],
          [3, 0],
          [4, 0],
          [5, 2, "1"],
          [6, 3, "2"],
          [1, 3, "3"],
        ],
      },
      {
        id: 6,
        name: "D",
        category: "Basic",
        fingers: [
          [4, 0],
          [5, "x"],
          [6, "x"],
          [1, 2, "2"],
          [3, 2, "1"],
          [2, 3, "3"],
        ],
      },
      {
        id: 7,
        name: "G7",
        category: "Seventh",
        fingers: [
          [2, 0],
          [3, 0],
          [4, 0],
          [1, 1, "1"],
          [5, 2, "2"],
          [6, 3, "3"],
        ],
      },
      {
        id: 8,
        name: "C7",
        category: "Seventh",
        fingers: [
          [1, 0],
          [6, "x"],

          [2, 1, "1"],
          [4, 2, "2"],
          [5, 3, "3"],
          [3, 3, "4"],
        ],
      },
      {
        id: 9,
        name: "B7",
        category: "Seventh",
        fingers: [
          [2, 0],

          [6, "x"],
          [4, 1, "1"],
          [5, 2, "2"],
          [3, 2, "3"],
          [1, 2, "4"],
        ],
      },
      {
        id: 10,
        name: "A7",
        category: "Seventh",
        fingers: [
          [1, 0],
          [3, 0],
          [5, 0],
          [6, "x"],
          [4, 2, "2"],
          [2, 2, "3"],
        ],
      },
      {
        id: 11,
        name: "D7",
        category: "Seventh",
        fingers: [
          [4, 0],
          [5, "x"],
          [6, "x"],
          [2, 1, "1"],
          [1, 2, "3"],
          [3, 2, "2"],
        ],
      },
      {
        id: 12,
        name: "E7",
        category: "Seventh",
        fingers: [
          [1, 0],
          [2, 0],
          [4, 0],
          [6, 0],
          [3, 1, "1"],
          [5, 2, "2"],
        ],
      },
      {
        id: 13,
        name: "Em7",
        category: "Minor Seventh",
        fingers: [
          [1, 0],
          [3, 0],
          [6, 0],
          [5, 2, "1"],
          [4, 2, "2"],
          [2, 3, "4"],
        ],
      },
      {
        id: 14,
        name: "Dm7",
        category: "Minor Seventh",
        fingers: [
          [4, 0],
          [5, "x"],
          [6, "x"],

          [3, 2, "2"],
        ],
        barre: [
          {
            fromString: 2,
            toString: 1,
            fret: 1,
          },
        ],
      },
      {
        id: 15,
        name: "Am7",
        category: "Minor Seventh",
        fingers: [
          [1, 0],
          [3, 0],
          [5, 0],
          [6, "x"],
          [2, 1, "1"],
          [4, 2, "2"],
        ],
      },
      {
        id: 16,
        name: "Esus",
        category: "Suspended",
        fingers: [
          [1, 0],
          [2, 0],
          [6, 0],
          [5, 2, "2"],
          [4, 2, "3"],
          [3, 2, "4"],
        ],
      },
      {
        id: 17,
        name: "Dsus",
        category: "Suspended",
        fingers: [
          [4, 0],
          [5, "x"],
          [6, "x"],
          [3, 2, "1"],
          [2, 3, "3"],
          [1, 3, "4"],
        ],
      },
      {
        id: 18,
        name: "Asus",
        category: "Suspended",
        fingers: [
          [1, 0],
          [5, 0],
          [6, "x"],
          [3, 2, "3"],
          [4, 2, "2"],
          [2, 3, "4"],
        ],
      },
    ],
    selected: [1, 2, 3, 4, 5, 6],
    current: [1, 2],
    locked: [],

    settings: {
      autoAdvance: false,
      showChart: true,
      roundDuration: 60,
      chordPracticeCount: 2,
    },
  },
  mutations: {
    SET_CHORDS(state, chords) {
      state.chords = chords;
    },
    SET_LOCKED_CHORDS(state, chords) {
      state.locked = chords;
    },

    SET_SELECTED_CHORDS(state, chords) {
      state.selected = chords;
    },
    SET_ROUND_DURATION(state, duration) {
      state.settings.roundDuration = duration;
    },
    SET_AUTO_ADVANCE(state, autoAdvance) {
      state.settings.autoAdvance = autoAdvance;
    },
    SET_SHOW_CHART(state, showChart) {
      state.settings.showChart = showChart;
    },
    SET_CURRENT_CHORDS(state, chords) {
      state.current = chords;
    },

    SET_CHORD_PRACTICE_COUNT(state, count) {
      state.settings.chordPracticeCount = count;
    },
    UPDATE_SELECTED(state, selected) {
      state.selected = selected;
    },
  },
  actions: {
    fetchChords({ commit }, chords) {
      commit("SET_CHORDS", chords);
    },
    lockChord({ commit }, id) {
      let lockedChords = [...this.state.locked, id];
      let uniqueChords = Array.from(new Set(lockedChords));
      commit("SET_LOCKED_CHORDS", uniqueChords);
    },
    unlockChord({ commit }, id) {
      let lockedChords = this.state.locked.filter((locked) => locked != id);
      commit("SET_LOCKED_CHORDS", lockedChords);
    },
    selectChord({ commit }, id) {
      let selectedChords = [...this.state.selected, id];
      let uniqueChords = Array.from(new Set(selectedChords));
      commit("SET_SELECTED_CHORDS", uniqueChords);
    },
    selectMultipleChords({ commit, getters }, ids) {
      let idsWithoutLocked = ids.filter((id) => !getters.isChordLocked(id));
      let selectedChords = [...this.state.selected, ...idsWithoutLocked];
      //Chords must be unique
      let uniqueChords = Array.from(new Set(selectedChords));
      commit("SET_SELECTED_CHORDS", uniqueChords);
    },
    unselectMultipleChords({ commit, getters }, ids) {
      let idsWithoutLocked = ids.filter((id) => !getters.isChordLocked(id));
      let selectedChords = this.state.selected.filter((selected) => {
        {
          return !idsWithoutLocked.includes(selected);
        }
      });
      commit("SET_SELECTED_CHORDS", selectedChords);
    },
    unSelectChord({ commit }, id) {
      let selectedChords = this.state.selected.filter(
        (selected) => selected != id
      );
      commit("SET_SELECTED_CHORDS", selectedChords);
    },
    unselectAllSelectedChords({ commit }) {
      let selectedChords = [];
      commit("SET_SELECTED_CHORDS", selectedChords);
    },
    setChordPracticeCount({ commit }, count) {
      commit("SET_CHORD_PRACTICE_COUNT", count);
    },
    updateSelected({ commit }, selected) {
      commit("UPDATE_SELECTED", selected);
    },
    setAutoAdvance({ commit }, autoAdvance) {
      commit("SET_AUTO_ADVANCE", autoAdvance);
    },
    setRoundDuration({ commit }, duration) {
      commit("SET_ROUND_DURATION", duration);
    },
    setShowChart({ commit }, showChart) {
      commit("SET_SHOW_CHART", showChart);
    },
    setRandomCurrentChords({ commit, state, getters }) {
      let currentChords = state.current;
      let newCurrentChords = [];
      let count = state.settings.chordPracticeCount;
      let allowableChords = state.selected.filter(
        (selected) => !getters.isChordLocked(selected)
      );
      console.log(allowableChords.length);
      if (count > allowableChords.length + 1) {
        newCurrentChords = currentChords;
      } else {
        for (let i = 0; i < count; i++) {
          if (i < currentChords.length) {
            if (getters.isChordLocked(currentChords[i])) {
              newCurrentChords.push(currentChords[i]);
              continue;
            }
          }
          let allowed = allowableChords.filter(
            (allowed) => !newCurrentChords.includes(allowed)
          );
          let randomChord = allowed[Math.floor(Math.random() * allowed.length)];
          newCurrentChords.push(randomChord);
        }
      }
      commit("SET_CURRENT_CHORDS", newCurrentChords);
    },
  },
  getters: {
    isChordLocked: (state) => (id) => {
      return state.locked.includes(id);
    },
    isChordSelected: (state) => (id) => {
      return state.selected.includes(id);
    },
    getSelectedChords: (state) => {
      const selectedChords = state.selected.map((selectedChord) => {
        return state.chords.find((chord) => chord.id == selectedChord);
      });
      return selectedChords;
    },
    getCurrentChords: (state) => {
      const currentChords = state.current.map((currentChord) => {
        return state.chords.find((chord) => chord.id == currentChord);
      });
      return currentChords;
    },
    getChordsMappedByCategory: (state) => {
      const chords = {};
      state.chords.forEach((chord) => {
        let category = chord.category;
        if (!(category in chords)) {
          chords[category] = [];
        }
        chords[category].push(chord);
      });

      return chords;
    },
    getChordsByCategory: (state) => (category) => {
      return state.chords.filter((chord) => chord.category == category);
    },
    areAllSelectedByCategory: (state, getters) => (category) => {
      let chords = getters.getChordsByCategory(category);
      return chords.every((chord) => {
        return getters.isChordSelected(chord.id);
      });
    },
    areNoneSelectedByCategory: (state, getters) => (category) => {
      let chords = getters.getChordsByCategory(category);
      return chords.every((chord) => {
        return !getters.isChordSelected(chord.id);
      });
    },
    getRoundDuration: (state) => {
      return state.settings.roundDuration;
    },
    getShowChart: (state) => {
      return state.settings.showChart;
    },
    getAutoAdvance: (state) => {
      return state.settings.autoAdvance;
    },
  },
});
new Vue({
  store: store,
  render: (h) => h(App),
}).$mount("#app");
