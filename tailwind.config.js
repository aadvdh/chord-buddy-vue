// tailwind.config.js
const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  theme: {
    extend: {
      customForms: (theme) => ({
        default: {
          checkbox: {
            "&:indeterminate": {
              background:
                "url(\"data:image/svg+xml,%3Csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3E%3Crect width='8' height='2' x='4' y='7' rx='1'/%3E%3C/svg%3E\");",
              borderColor: "transparent",
              backgroundColor: "currentColor",
              backgroundSize: "100% 100%",
              backgroundPosition: "center",
              backgroundRepeat: "no-repeat",
            },
          },
        },
      }),
    },
  },
  variants: {
    extend: {
      opacity: ["disabled"],
    },
  },
  purge: ["./src/**/*.html", "./src/**/*.vue"],
  plugins: [require("@tailwindcss/ui"), require("@tailwindcss/custom-forms")],
};
